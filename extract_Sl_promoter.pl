#!/usr/bin/perl -w
$ENV{BIOPERL_INDEX_TYPE} = "SDBM_File";

#script to extract upstream regions from genes, given fasta and gff3 files
#updated May 18 to use ITAG2.3

use Bio::FeatureIO;
use List::Util qw(max min);
use Bio::SeqIO;
use Bio::DB::Fasta;

my $verbose = 0;


if (scalar @ARGV == 0)  {
	$upstream = 1000; #the amount of sequence to extract, in bp
} else {
	$upstream = $ARGV[0];
}

#file containing information about gene locations
$gff_file = "/Volumes/SolexaRAID/Solexa_runs_Data/00.Downloaded_references_and_others/S_lycopersicum_ITAG2.3/ITAG2.3_gene_models.gff3";

#file with chromosomes
$chromosome_file = "/Volumes/SolexaRAID/Solexa_runs_Data/00.Downloaded_references_and_others/S_lycopersicum_chromosomes.2.40.fa";

#file where output will be written
$outfile = "SlUpstream".$upstream.".fa";

#open gff file for reading
$gffIO = Bio::FeatureIO->new(-file => $gff_file, -format => 'gff', -version => 3);

#open chromosome file for reading
#line below probably not needed; switiching to database access
#$chromosome_in = Bio::SeqIO->new(-file => $chromosome_file, -format => 'fasta');

$chromosome_db = Bio::DB::Fasta->new($chromosome_file);

#open output file for wriing
$prom_out = Bio::SeqIO->new(-file => ">".$outfile, -format => 'fasta');

#loop through each feature in the gff file
while ($feature = $gffIO->next_feature()) {
	#look for features defining regions (chromosomes) and get length
	if ($feature->primary_tag() eq "region") {
		if ($verbose) {print "\n region is true \n";}
		#get chromosome sequence for new chromosome
		$chromosomeID = $feature->seq_id();
		#using 2.31 chromosomes; need to convert name
		$chromosomeID =~ s/30/31/;
		$chromosome = $chromosome_db->get_Seq_by_id($chromosomeID);
		if ($gene) { # we have a left-over gene from the last region
			if ($verbose) {print "leftover gene is true \n";}
			&getPromoter;
			&writePromoter;			
			
			if ($verbose) {print "name: ".$IDfields[1] ." type: ". $gene->primary_tag().
				" start: ". $prom_start ." end: ". $prom_end."\n";}
			$gene = undef;
		}# if gene
		$limit5prime = 1;
		$region_end = $feature->end(); #get chromosome end if current feature is a chromosome
	}# if region
	
	#look for mRNA features...this is what we want
	#keep info on two genes at a time so that we can establish mRNA boundaries and truncate promoters
	if ($feature->primary_tag() eq "mRNA") { 
		if ($verbose) {print "\n mRNA is true\n";}
		#in this round we define the promoter for $gene
		#we use $feature to determine the 3' limit of the promoter,
		#in case $gene is on the minus strand
		
		#define 3' limit based on feature
		
		$limit3prime = $feature->start();
		
		if ($gene) {# if not first gene of region then get promoter
						  # otherwise, just shift
		    if ($verbose) {print "not first gene\n";}
			&getPromoter;
			&writePromoter;
			if ($verbose) {print "name: ".$IDfields[1] ." type: ". $gene->primary_tag.
							" start: ". $prom_start ." end: ". $prom_end."\n";}
			$limit5prime = $gene->end(); #the end of the current gene is the 5' boundary 
										 #for the next promoter region
		}# if $gene
		
		$gene = $feature;
		$limit3prime = $region_end; # needed for the "leftover" gene when starting a new region
	} #if mRNA
} #while

if ($gene) {
	#write the final gene
	&getPromoter;
	&writePromoter;
}

sub getPromoter{
		#promoter position depends on the strand
	if ($gene->strand() == 1) {
		 #ensure that we stay in bounds and truncate based on upstream gene
		$prom_start = max($gene->start() - $upstream,$limit5prime);			
		$prom_end = $gene->start();
	} else { # gene is on reverse strand
		#ensure that we stay in bounds and do not run into next gene
		$prom_start = min($gene->end() + $upstream, $limit3prime, $region_end); 
		$prom_end = $gene->end();
	} # else
	#get name of gene
	@ID = $gene->get_tag_values("ID");
	@IDfields = split( /:/, $ID[0]);
}#getPromoter
			
sub writePromoter{
	if ($prom_start < $prom_end) {
		if ($verbose) {print "trying to extract from $prom_start to $prom_end from $chromosomeID \n";}
		$prom_seq = $chromosome->subseq($prom_start => $prom_end);
		$prom_obj = Bio::Seq->new(-seq => $prom_seq,
								-display_id => $IDfields[1],
								-desc => "promoter$upstream $prom_start $prom_end");
	} else {#need to switch start and end and then revcomp
		$prom_seq = $chromosome->subseq($prom_end => $prom_start);
		$prom_obj = Bio::Seq->new(-seq => $prom_seq,
								-display_id => $IDfields[1],
								-desc => "promoter$upstream $prom_start $prom_end");
		$prom_obj = $prom_obj->revcom;
	} 
	$prom_out->write_seq($prom_obj);
}